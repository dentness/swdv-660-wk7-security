import io
from Crypto.Cipher import AES
from Crypto.Hash import SHA256
from Crypto import Random
from Crypto.PublicKey import RSA


"""
SWDV 660 2W 19/SU1
Joe Dent
Week 7 Assignment
"""


def sym_encrypt(secret: str, content: str) -> bytes:
    """Symmetric Encryption

    Args:
        secret: The symmetric key used to encrypt
        content: String to be encrypted

    Returns:
        The AES encrypted content

    """
    outbuffer = io.BytesIO()
    inbuffer = io.BytesIO(bytes(content, 'utf-8'))

    chunksize = 16 * 1024
    datasize = str(len(bytes(content, 'utf-8'))).zfill(16)
    IV = Random.new().read(16)

    key = SHA256.new(secret.encode('utf-8'))

    enc = AES.new(key.digest(), AES.MODE_CBC, IV)
    # Write out file header: Size and Salt
    outbuffer.write(datasize.encode('utf-8'))
    outbuffer.write(IV)

    while True:
        chunk = inbuffer.read(chunksize)
        if len(chunk) == 0:
            break
        elif len(chunk) % 16 != 0:
            chunk += b'\x00' * (16 - (len(chunk) % 16))
        outbuffer.write(enc.encrypt(chunk))

    outbuffer.flush()
    return outbuffer.getvalue()


def sym_decrypt(secret: str, cipher_text: bytes) -> str:
    """Symmetric Decryption

    Args:
        secret: The symmetric key used to decrypt
        cipher_text: Bytes to be decrypted

    Returns:
        The AES encrypted content

    """
    outbuffer = io.BytesIO()
    inbuffer = io.BytesIO(cipher_text)

    chunksize = 16 * 1024

    # Read the header
    datasize = int(inbuffer.read(16))  # Not needed here, but need to remove it from the stream.
    IV = inbuffer.read(16)

    key = SHA256.new(secret.encode('utf-8'))
    dec = AES.new(key.digest(), AES.MODE_CBC, IV)

    while True:
        chunk = inbuffer.read(chunksize)

        if len(chunk) == 0:
            break

        outbuffer.write(dec.decrypt(chunk))

    outbuffer.flush()
    msg = outbuffer.getvalue()
    return str(msg[:datasize], 'utf-8')


def asym_encrypt(key: str, content: str) -> bytes:
    """Asymmetric Encryption

    Args:
        key: The asymmetric key used to encrypt
        content: String to be encrypted

    Returns:
        The RSA encrypted content

    """
    key_obj = RSA.importKey(key)
    encrypted_msg = key_obj.encrypt(content.encode('UTF-8'), 32)[0]
    return encrypted_msg


def asym_decrypt(key: str, cipher_text: str) -> str:
    """Asymmetric Decryption

    Args:
        key: The asymmetric key used to decrypt
        cipher_text: String to be decrypted

    Returns:
        The AES encrypted content

    """
    key_obj = RSA.importKey(key)
    decrypted_msg = key_obj.decrypt(cipher_text)
    return decrypted_msg.decode('UTF-8')
