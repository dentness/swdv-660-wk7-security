import socket
import logging

"""
SWDV 660 2W 19/SU1
Joe Dent
Week 7 Assignment
"""

# Constants
ADDRESS = ''
DEFAULT_BUFFER_SIZE = 4096
MAX_CONNECTIONS = 100
PORT = 8081


def generate_response(cert: bytes) -> bytes:
    """call a service with the provided msg.

    Args:
        cert: The cert sent to the lookup service.

    Returns:
        The public key associated with the cert
        If cert not found, return empty result

        Full implementation would allow for registering a cert
        and storing in a more durable resource.

    """
    PUB_KEY = b'-----BEGIN PUBLIC KEY-----\nMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCjjdyiZSPGzYqJR0ZtBr4SJqcj\nLaz+ts3gfa6mB4yoNUCzuO+0wVcV0C3dakKBxs9ji28epIp9kZ3b0WDjo1IZVSL6\nf+9yUp9T09SZpZD9MXLDYe8T5iZ0/+7DHTLZZ+ieH1NEji+/XHvoY9e9AqXOAKVS\nXyKxCpaBUMBwH1Kz3wIDAQAB\n-----END PUBLIC KEY-----'

    if cert == b'ca096f9c-22b6-49a5-8f8c-26f38cff6f28':
        return PUB_KEY

    return b''


def listen_and_handle_requests(svr: socket) -> None:
    """listen for requests and process responses

    Args:
        svr: The socket server to listen on.


    Returns:
        None

    """
    svr.listen(MAX_CONNECTIONS)
    while True:
        conn, addr = svr.accept()

        # Get the cert to lookup a key
        cert = conn.recv(DEFAULT_BUFFER_SIZE)
        logging.info('Looking up: ' + cert.decode('utf-8'))

        # Return the key found or empty bytes if not found
        conn.send(generate_response(cert))
        conn.close()


def main():
    # Configure server
    s = socket.socket()
    s.bind((ADDRESS, PORT))
    logging.basicConfig(level=logging.INFO, filename='authority.log', filemode='w')

    # Process requests
    listen_and_handle_requests(s)


if __name__ == "__main__":
    main()
