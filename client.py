import socket
import uuid
import logging
import crypto

"""
SWDV 660 2W 19/SU1
Joe Dent
Week 7 Assignment
"""

# constants
ADDRESS = ''
DEFAULT_BUFFER_SIZE = 4096
DEFAULT_ENCODING = 'UTF-8'
PORT = 9500

CA_ADDRESS = ''
CA_PORT = 8081


def gen_session_cipher_key() -> str:
    """Generate a unique cipher key with each call"""
    return uuid.uuid4().hex


def verify_cert(cert: str) -> str:
    """Call CA and verify cert.  If cert is verified, expect public key returned """
    logging.basicConfig(level=logging.INFO)

    s = socket.socket()
    s.connect((CA_ADDRESS, CA_PORT))
    s.send(cert.encode(DEFAULT_ENCODING))
    return s.recv(DEFAULT_BUFFER_SIZE).decode(DEFAULT_ENCODING)


def establish_secured_connection(addr, port):
    """ Provide initial SSL handshake """
    logging.info("Establishing a connection:  ")
    s = socket.socket()
    s.connect((addr, port))
    s.send(b'HELLO')

    cert = s.recv(DEFAULT_BUFFER_SIZE).decode(DEFAULT_ENCODING)
    logging.info('certificate: ' + cert)

    pub_key = verify_cert(cert)
    if len(pub_key) > 0:
        logging.info('certificate verified')
        cipher_key = gen_session_cipher_key()
        logging.info('cipher pub_key: ' + cipher_key)
        e = crypto.asym_encrypt(pub_key, cipher_key)
        s.send(e)

        ack = s.recv(DEFAULT_BUFFER_SIZE)
        if crypto.sym_decrypt(cipher_key, ack) == 'ENCRYPTED':
            logging.info('Established secured connection.')
            return s, cipher_key

    logging.info('Failed to establish secured connection.')
    s.send(b'GOODBYE')
    return None, None


def secured_call(msg: str, addr: str, port: int) -> str:
    """call a service with the provided msg.

    Args:
        msg: The body messsage sent to the service.
        addr: Address of the service.
        port: Port of the service, as an int.

    Returns:
        The decoded message from the service across
        an SSL-like exchange, with reversed content.

    """

    connection, cipher = establish_secured_connection(addr, port)
    if connection is None:
        return

    if len(cipher) == 0:
        logging.info('Failed to obtain symmetric key.')
        connection.send(b'GOODBYE')
        return

    logging.info('Ready for data exchange.')
    enc_msg = crypto.sym_encrypt(cipher, msg)
    connection.send(enc_msg)

    cipher_text = connection.recv(DEFAULT_BUFFER_SIZE)
    logging.info(cipher_text)

    response = crypto.sym_decrypt(cipher, cipher_text)
    print(response)


def main():
    logging.basicConfig(level=logging.INFO, filename='client.log', filemode='w')
    secured_call('free willie', ADDRESS, PORT)
    secured_call('This is a test of the emergency broadcast system.', ADDRESS, PORT)
    secured_call('Some days are better than others', ADDRESS, PORT)
    secured_call('1234567890', ADDRESS, PORT)
    secured_call('AbCdEfGhIjKlMnOp', ADDRESS, PORT)


if __name__ == '__main__':
    main()
