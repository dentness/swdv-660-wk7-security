import socket
import logging
import crypto
from typing import Optional

"""
SWDV 660 2W 19/SU1
Joe Dent
Week 7 Assignment
"""

# Constants
ADDRESS = ''
DEFAULT_BUFFER_SIZE = 4096
DEFAULT_ENCODING = 'UTF-8'
MAX_CONNECTIONS = 100
PORT = 9500
CERT = b'ca096f9c-22b6-49a5-8f8c-26f38cff6f28'
PRV_KEY = '-----BEGIN RSA PRIVATE KEY-----\nMIICWwIBAAKBgQCjjdyiZSPGzYqJR0ZtBr4SJqcjLaz+ts3gfa6mB4yoNUCzuO+0\nwVcV0C3dakKBxs9ji28epIp9kZ3b0WDjo1IZVSL6f+9yUp9T09SZpZD9MXLDYe8T\n5iZ0/+7DHTLZZ+ieH1NEji+/XHvoY9e9AqXOAKVSXyKxCpaBUMBwH1Kz3wIDAQAB\nAoGAfrdysf9acLTfGO04C1CmArof1ImW9uH7MGsv3UMMH2bi8lvenFUPe78HSf0B\n+jpmg+GL/lnNUux3OdBHGRd4ixjg5UyRWcmlE61xtRcvj55wHBnr9h1XKYhqqV7e\nP/ym2I98CZ+140LSP4asq5CHOnkx0zp3/8bv9ZM/1oA3lwECQQC2lSdBnvAE1sIk\ny15Girm9E7vY5SgCxXJAuJ3T92LzK6/pJ9pjCOygHaSMKUReW9Uy8z5+xGEPsRwp\nlPfgs1sxAkEA5VHwN02c/WdMA6EynTyEzkbv7f6WX58XKHZO3NtP7CHw4c7XsmTh\n24TFrE0iTP33rAkOSegAXbc68owFQIccDwJAYWqNana5y2smZOlZILwtw9M+m8gQ\npAHjJuMZ2pmY4vPBRN5tp/9/+SI3xEzUXKAUzb1477wpqWyvAQSpiOtJUQJAR7wj\nDdm8k1R95eDGKYOl87UjFur99TmaD/E6aEI+tRpE9Q40UTzXnkjhYS3b3gPQN0O5\n8YqemrJXwUnl1rKtJwJAT/CQWLkguh13lM7wOUgNTD1/QFOCzkGhkk3SvDm9IAhX\nawdQ+23ud1rxyACkysPgJ9GgRe9iZzpo9Oiz0xtGyg==\n-----END RSA PRIVATE KEY-----'
PUB_KEY = '-----BEGIN PUBLIC KEY-----\nMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCjjdyiZSPGzYqJR0ZtBr4SJqcj\nLaz+ts3gfa6mB4yoNUCzuO+0wVcV0C3dakKBxs9ji28epIp9kZ3b0WDjo1IZVSL6\nf+9yUp9T09SZpZD9MXLDYe8T5iZ0/+7DHTLZZ+ieH1NEji+/XHvoY9e9AqXOAKVS\nXyKxCpaBUMBwH1Kz3wIDAQAB\n-----END PUBLIC KEY-----'


def reverse(msg: str) -> str:
    """Simple processing function to return the reverse of the string passed in

    Args:
        msg: String to be processed

    Returns:
        The reverse of the msg string

    """
    return msg[::-1]


def receive_init_msg(conn) -> bool:
    """
        Listen for requests and initiate handshake or stop the server
        Only accept HELLO or GOODBYE verbs

    Args:
        conn: The socket server to listen on.

    Returns:
        True - Initiate Handshake
        False - Signal to stop the server

    """
    logging.info('receive_init_msg')

    while True:
        msg = conn.recv(DEFAULT_BUFFER_SIZE)
        if msg == b'HELLO':
            conn.send(CERT)
            return True
        if msg == b'GOODBYE':
            return False

        logging.info('receive_init: ignoring odd text: ' + msg.decode(DEFAULT_ENCODING))


def receive_cipher(conn) -> Optional[str]:
    """listen for requests and process responses

    Args:
        conn: The socket server to listen on.

    Returns:
        Cipher - proper symmetric key for decrypting content
        None - a cipher could not be generated, so exiting

    """
    cipher_text = conn.recv(DEFAULT_BUFFER_SIZE)
    logging.info(b'receive_cipher: ' + cipher_text)

    if cipher_text.upper() == 'GOODBYE':
        return None

    # Decrypt cipher with Private Key
    cipher = crypto.asym_decrypt(PRV_KEY, cipher_text)

    # Acknoledge receipt of cipher key
    conn.send(crypto.sym_encrypt(cipher, 'ENCRYPTED'))

    return cipher


def listen_and_handle_requests(svr: socket) -> None:
    """listen for requests and process responses

    Args:
        svr: The socket server to listen on.

    Returns:
        None

    """
    svr.listen(MAX_CONNECTIONS)

    while True:
        # Wait for handshake to start
        conn, addr = svr.accept()
        success = receive_init_msg(conn)
        if not success:
            logging.info('Failed to init connection... panic!')
            conn.close()
            break

        # Receive the cipher for this exchange
        cipher = receive_cipher(conn)
        if cipher is not None:  # only process if cipher returned
            logging.info('Ready to receive content')

            # Get the message to process and decrypt with cipher
            enc_msg = conn.recv(DEFAULT_BUFFER_SIZE)
            logging.info(b'Received encrypted message: ' + enc_msg)
            msg = crypto.sym_decrypt(cipher, enc_msg)
            logging.info('Decrypted Message: ' + msg)

            # Return processed message encrypted with cipher
            conn.send(crypto.sym_encrypt(cipher, reverse(msg)))

        conn.close()


def main():
    # Configure server
    s = socket.socket()
    s.bind((ADDRESS, PORT))
    logging.basicConfig(level=logging.INFO, filename='server.log', filemode='w')

    # Process requests
    listen_and_handle_requests(s)


if __name__ == "__main__":
    main()
