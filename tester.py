import crypto
import uuid

from Crypto.PublicKey import RSA
from Crypto import Random

ADDRESS = ''
DEFAULT_BUFFER_SIZE = 4096
DEFAULT_ENCODING = 'UTF-8'
PORT = 9500

CA_ADDRESS = ''
CA_PORT = 8081

PRV_KEY = '-----BEGIN RSA PRIVATE KEY-----\nMIICWwIBAAKBgQCjjdyiZSPGzYqJR0ZtBr4SJqcjLaz+ts3gfa6mB4yoNUCzuO+0\nwVcV0C3dakKBxs9ji28epIp9kZ3b0WDjo1IZVSL6f+9yUp9T09SZpZD9MXLDYe8T\n5iZ0/+7DHTLZZ+ieH1NEji+/XHvoY9e9AqXOAKVSXyKxCpaBUMBwH1Kz3wIDAQAB\nAoGAfrdysf9acLTfGO04C1CmArof1ImW9uH7MGsv3UMMH2bi8lvenFUPe78HSf0B\n+jpmg+GL/lnNUux3OdBHGRd4ixjg5UyRWcmlE61xtRcvj55wHBnr9h1XKYhqqV7e\nP/ym2I98CZ+140LSP4asq5CHOnkx0zp3/8bv9ZM/1oA3lwECQQC2lSdBnvAE1sIk\ny15Girm9E7vY5SgCxXJAuJ3T92LzK6/pJ9pjCOygHaSMKUReW9Uy8z5+xGEPsRwp\nlPfgs1sxAkEA5VHwN02c/WdMA6EynTyEzkbv7f6WX58XKHZO3NtP7CHw4c7XsmTh\n24TFrE0iTP33rAkOSegAXbc68owFQIccDwJAYWqNana5y2smZOlZILwtw9M+m8gQ\npAHjJuMZ2pmY4vPBRN5tp/9/+SI3xEzUXKAUzb1477wpqWyvAQSpiOtJUQJAR7wj\nDdm8k1R95eDGKYOl87UjFur99TmaD/E6aEI+tRpE9Q40UTzXnkjhYS3b3gPQN0O5\n8YqemrJXwUnl1rKtJwJAT/CQWLkguh13lM7wOUgNTD1/QFOCzkGhkk3SvDm9IAhX\nawdQ+23ud1rxyACkysPgJ9GgRe9iZzpo9Oiz0xtGyg==\n-----END RSA PRIVATE KEY-----'
PUB_KEY = '-----BEGIN PUBLIC KEY-----\nMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCjjdyiZSPGzYqJR0ZtBr4SJqcj\nLaz+ts3gfa6mB4yoNUCzuO+0wVcV0C3dakKBxs9ji28epIp9kZ3b0WDjo1IZVSL6\nf+9yUp9T09SZpZD9MXLDYe8T5iZ0/+7DHTLZZ+ieH1NEji+/XHvoY9e9AqXOAKVS\nXyKxCpaBUMBwH1Kz3wIDAQAB\n-----END PUBLIC KEY-----'


def main():
    symkey = uuid.uuid4().hex
    sym_enc = crypto.sym_encrypt(symkey, 'ABC')
    print(type(sym_enc))
    print(sym_enc)
    sym_decrypt = crypto.sym_decrypt(symkey, sym_enc)
    print(type(sym_decrypt))
    print(sym_decrypt)

    asym_enc = crypto.asym_encrypt(PUB_KEY, 'ABC')
    print(type(asym_enc))
    print(asym_enc)
    asym_decrypt = crypto.asym_decrypt(PRV_KEY, asym_enc)
    print(type(asym_decrypt))
    print(asym_decrypt)


def newmain():
    random_gen = Random.new().read
    key = RSA.generate(1024, random_gen)

    print(key.exportKey())
    print(key.publickey().exportKey())

    public_key = key.publickey()
    enc_data = public_key.encrypt('abcdefgh'.encode('UTF-8'), 32)

    print(key.decrypt(enc_data))


if __name__ == "__main__":
    main()
